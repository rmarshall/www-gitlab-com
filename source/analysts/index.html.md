---
layout: markdown_page
title: Analyst Relations at GitLab
description: Insight about AR at GitLab
---

## What are analysts saying about GitLab and how are we building off their insight and input?

## Where does GitLab Engage?

GitLab appears in several areas within the analyst arena.  Areas we engage analysts include:

  - Source Code Management (SCM)
  - Release Automation
  - Continuous Integration and Continuous Delivery (CI/CD)
  - Value Stream Management (VSM)
  - Application Security (AppDevSec)
  - Continuous Deployment and Release Automation (CDRA)
  - DevOps
  - Planning/Project Management
  - Agile Project management

## GitLab in recent reports

Here are links to recent reports where GitLab has been covered or mentioned.

  - The Forrester Wave™: Continuous Integration Tools, Q3 2017
  - IDC Innovators: Agile Code Development Technologies 2018   

## GitLab open issues

TBD